let spanInput = document.createElement('label');
spanInput.className = 'input-container';
spanInput.innerHTML = 'Введи цену: ';
spanInput.style.display = 'block';
let priceInput = document.createElement('input');
let textPrice = document.createElement('p');
let userPrice = document.createElement('div');
userPrice.className = 'price-now';
userPrice.style.display = 'inline-block';
let closeButton = document.createElement('button');
closeButton.className = 'close-button';
closeButton.innerHTML = '<b>x</b>';
priceInput.onfocus = () => {
    priceInput.style.cssText = "border: 2px solid green;";
};

        let error = document.createElement('p');
        error.innerHTML = 'Введи правильное число!';
        error.style.color = 'red';

priceInput.onblur = () => {
    document.body.insertBefore(userPrice, spanInput);
    userPrice.innerHTML = `Текущая цена: ${priceInput.value}`;
    userPrice.appendChild(closeButton);

    //    userPrice.style.display ='';

    if (priceInput.value <= 0) {
        document.body.appendChild(error);
        priceInput.style.borderColor = 'red';
        priceInput.style.color = 'red';
        userPrice.remove();
    } else {
        priceInput.style.color = 'green';
        error.remove();
    }
};

closeButton.onclick = () => {
    userPrice.remove();
    priceInput.value = "";
    error.remove();
}
spanInput.appendChild(priceInput);
document.body.appendChild(spanInput);
