//Циклы необходимы для того что б выполнить какое-то (одинаковое) действие несколько раз, при этом не писать код вручную.
//
//let numberM = +prompt('Введите число "m":', '')
//if ( numberM < 5){
//    alert('sorry no numbers');
//} else {
//
//for (let i = 0; i < numberM; i++) {
//    if (i % 5 !== 0) continue;
//    
//    console.log(i)
//}
//}


//--------------------------------------------------Задание 2.2 (while)
let numberM = +prompt('Введите число "m":', ''),
    numberN = +prompt('Введите число "n":', '')
//
////проверяю циклом на больше/меньше, на целое число
while (numberM > numberN || !Number.isInteger(numberM) || !Number.isInteger(numberN) ) {
    alert('Либо Вы ввели не целые числа, либо число М больше числа N')
    numberM = +prompt('Введите ЕЩЕ РАЗ число "m":', '');
    numberN = +prompt('Введите ЕЩЕ РАЗ число "n":', '');
}
////присваиваю i число введенное пользователем
//let i = numberM
//
////запускаю цикл
//while (i < numberN) {
//    console.log(i);
//    i++



nextPrime:
  for (var i = numberM; i <= numberN; i++) {

    for (var j = 2; j < i; j++) {
      if (i % j == 0) continue nextPrime;
    }

    console.log( i ); // простое
  }
//}

