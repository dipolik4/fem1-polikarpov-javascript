let tabs = document.getElementById('tabs'),
    tabsContent = document.getElementById('tabsContent');

for (let i=0; i<tabs.children.length; i++) {
    tabs.children[i].dataset.numberOfTab = i;
}

for(let i=1; i<tabsContent.children.length; i++){
    tabsContent.children[i].style.display = 'none';
}

tabs.onclick = () =>{
document.querySelector('.active').classList.remove('active');
event.target.classList.add('active');
    for(let i=0; i<tabsContent.children.length; i++){
    tabsContent.children[i].style.display = 'none';
};
tabsContent.children[event.target.dataset.numberOfTab].style.display = 'block';
};