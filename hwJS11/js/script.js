const btn = document.body.querySelectorAll('.btn');

document.body.onkeypress = (event) => {
    const userKey = event.key;
    btn.forEach((button) => {
        console.log(button.innerHTML);
        if (userKey.toUpperCase() === button.innerHTML.toUpperCase()) {
            button.style.backgroundColor = 'blue';
            console.log(button);
        } else {
            button.style.backgroundColor = '#33333a';
        }
    });
};