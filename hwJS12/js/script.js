let imagesList = document.querySelectorAll('.image-to-show');
let stopButton = document.createElement('button');
stopButton.innerHTML = 'Прекратить';
document.body.appendChild(stopButton);
let goButton = document.createElement('button');
goButton.innerHTML = 'Продолжить';
document.body.appendChild(goButton);

imagesList.forEach((elem) => {
    elem.style.display = 'none';
})
let index = 0;
imagesList[index].style.display = 'block';

function startSlider() {
    imagesList[index].style.display = 'none';
    index++;
    if (index === imagesList.length) {
        index = 0;
    };
    imagesList[index].style.display = 'block';
};

let timerId = setInterval(startSlider, 1000);

stopButton.onclick = () => {
    clearInterval(timerId);
};

goButton.onclick = () => {
    clearInterval(timerId);
    timerId = setInterval(startSlider, 1000);

};
