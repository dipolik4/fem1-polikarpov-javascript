let $btnUp = $('#btnup');
let screenHeight = document.documentElement.clientHeight;

$('.top-nav').on('click', 'a', function (e) {
    e.preventDefault();
    const id = $(this).attr('href'),
        top = $(id).offset().top;
    $('body,html').animate({
        scrollTop: top
    }, 900);
});



$btnUp.hide();
setInterval(function () {
    if (window.pageYOffset > screenHeight) {
        $btnUp.show(500);
    } else {
        $btnUp.hide(300);
    }
}, 900);

$btnUp.on('click', () => {
    window.scrollTo({
        top: 0,
        left: 0,
        behavior: 'smooth'
    })
})

$('#hide-blog').on('click', function () {
    $('section.news').slideToggle();
});

$('#hide-blog').on('click', function () {
    if ($(this).hasClass('change-text-btn')) {
        $(this).text('Show blog');
        $(this).removeClass('change-text-btn');
    } else {
        $(this).addClass('change-text-btn');
        $(this).text('Hide blog');
    }
    });
