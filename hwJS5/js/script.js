function createNewUser() {
    const newUser = {
        name: prompt('name', 'Dima'),
        lastname: prompt('lastname', 'Lol'),
        birthday: prompt('enter your B-day', '31.05.2010'),

        getLogIn: function () {
            return (this.name.charAt(0) + '.' + this.lastname).toLowerCase()
        },
        
        getAge: function () {
            let userDateArr = this.birthday.split('.');
            let userAge = (+new Date().getFullYear()) - (+userDateArr[2]);


            if ((+userDateArr[1]) > (new Date().getMonth() + 1)) {
                alert('Вам ' + (+userAge - 1) + ' лет');
            } else if ((+userDateArr[0]) > new Date().getDate()) {
                alert('Вам ' + (+userAge - 1) + ' лет');
            } else {
                alert('Вам ' + (+userAge) + ' лет');
            }
        },
        
        getPassword: function () {
            return this.name.charAt(0).toUpperCase() + this.lastname.toLowerCase() + this.birthday.substr(6,4)
        } 
    };
    return newUser
}

let mainUser = createNewUser();
console.log(mainUser);
console.log(mainUser.getLogIn());
mainUser.getAge();
console.log(mainUser.getPassword());
