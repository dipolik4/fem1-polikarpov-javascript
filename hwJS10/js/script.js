let allInputs = document.getElementsByTagName('input');
let buttonPass = document.getElementsByTagName('i');
let checkButton = document.querySelector('.btn');

[].forEach.call(buttonPass, a => {
    a.classList.remove('fa-eye');
    a.classList.add('fa-eye-slash');
});

const buttonShow = document.querySelectorAll('.fa-eye-slash');

buttonShow.forEach((element) => {
    element.addEventListener('click', showPass);
});

function showPass() {
    let inp1 = allInputs[0].getAttribute('type');
    let inp2 = allInputs[1].getAttribute('type');

    if (inp1 === 'password' && inp2 === 'password') {
        for (i = 0; i < allInputs.length; i++) {
            allInputs[i].type = 'text';
        };
        buttonShow.forEach((a) => {
            a.classList.toggle('fa-eye');
            a.classList.toggle('fa-eye-slash');
        });
    } else {
        for (i = 0; i < allInputs.length; i++) {
            allInputs[i].type = 'password';
        };
        buttonShow.forEach((a) => {
            a.classList.toggle('fa-eye');
            a.classList.toggle('fa-eye-slash');
        });

    }
};

checkButton.addEventListener('click', checkPass);

function checkPass() {
    if (allInputs[0].value === allInputs[1].value) {
        alert('Добро Пожаловать!');
    } else {
        let alarmText = document.createElement('p');
        alarmText.innerHTML = 'Ваши пароли не совпадают!';
        alarmText.style.color = 'red';
        let label = document.querySelectorAll('.input-wrapper')
        label[1].insertBefore(alarmText, buttonPass[1]);
    };
}
