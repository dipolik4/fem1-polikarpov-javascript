let userName = prompt('Put your name please', 'Имя');
let userAge = +prompt('Put your age please', '');

while (userName === '' || !isNaN(userName)) {
    alert('Name input field is not correctly filled');
    userName = prompt('Put your name again please', userName);
}

while (userAge == 0 || userAge == "" || isNaN(userAge)) {
    alert("You already put not a number, or you put 0");
    userAge = +prompt('Put your age again please!', userAge);
}

if (userAge < 18) {
    alert('Sorry ' + userName + ', you are not allowed to visit this website');
} else if (userAge <= 22) {
    if (confirm('Are you sure you want to continue?')) {
        alert('Welcome ' + userName)
    } else {
        alert('Sorry ' + userName + ', you are not allowed to visit this website')
    }
} else {
    alert("Welcome " + userName);
}
